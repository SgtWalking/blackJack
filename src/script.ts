/*
===============================================================================
Variables
===============================================================================
*/
const SCORE_MAX: number = 21;
// Number of decks to play with
const DECK_NUM: number = 1;
let end: boolean = false;
const SUITS: Suit[] = ['♠', '♥', '♣', '♦'];
const FACES: Face[] = [
  'A',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  'J',
  'Q',
  'K',
];

/*
===============================================================================
DOM elements
===============================================================================
*/
const $d: Document = document;
const $btn: HTMLButtonElement = $d.querySelector('#b');
const $end: HTMLButtonElement = $d.querySelector('#e');
$end.disabled = true;
const $score: HTMLElement = $d.querySelector('#score');
const $cards: HTMLElement = $d.querySelector('#cards');

/*
===============================================================================
Classes and types
===============================================================================
*/
type Suit = '♠' | '♥' | '♣' | '♦';
type Face =
  | 'A'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | 'J'
  | 'Q'
  | 'K';

/**
 * Card class
 */
class Card {
  suit: Suit;
  face: Face;
  value: number;

  /**
   * Card class constructor
   * @param {Suit} suit Suit of the card
   * @param {Face} face Face of the card
   */
  constructor(suit: Suit, face: Face) {
    this.suit = suit;
    this.face = face;
  }

  /**
   * Get card value from face of card
   * @param {boolean} isAceOne Choose if Ace should be counted as 1 or 11
   */
  getValue = (isAceOne: boolean = true): number => {
    switch (this.face) {
      case 'A':
        return isAceOne ? 1 : 11;
      case 'J':
      case 'Q':
      case 'K':
        return 10;
      default:
        return parseInt(this.face);
    }
  }
}

// Generate card deck(s)
const CARDS: Card[] = [];
for (let i: number = 0; i < DECK_NUM * FACES.length; i++)
  for (let j: number = 0; j < DECK_NUM * SUITS.length; j++) {
    CARDS.push(new Card(SUITS[j], FACES[i]));
  }

/**
 * Player class
 */
class Player {
  score: number;
  cards: Card[];
  draws: number;

  constructor() {
    this.score = 0;
    this.cards = [];
    this.draws = 0;
  }

  /**
   * Build card display string
   */
  display = () => {
    let out: string = '';
    this.cards.forEach((card: Card) => {
      out += card.suit + card.face + ' ';
    });
    return out;
  }
}
let player: Player = new Player();
let dealer: Player = new Player();

/*
===============================================================================
Functions
===============================================================================
*/
/**
 * Reset the game after it's over
 */
const reset = (): void => {
  player = new Player();
  dealer = new Player();
  end = false;

  $score.innerHTML = '0 - 0';
  $cards.innerHTML = 'x - x';

  $btn.innerHTML = 'Draw a card';
  $btn.removeEventListener('click', reset);
};

/**
 * Render scores and enabled end button
 */
const render = (): void => {
  $cards.innerHTML = player.display() + ' - ' + dealer.display();
  $score.innerText = player.score + ' - ' + dealer.score;
  if (player.score > 0) $end.disabled = false;
};

/**
 * Renders won, lost or draw string and disables draw and end buttons
 * @param {Player} winner Winner
 */
const win = (winner: Player): void => {
  winner
    ? ($score.innerHTML += winner === player ? ' - Won' : ' - Lost')
    : ($score.innerHTML += ' - Draw');

  $end.disabled = true;
  $btn.innerHTML = 'Reset';
  $btn.addEventListener('click', reset);
};

/**
 * Checks to determine the winner
 */
const checkWinner = (): void => {
  if (player.score > 21) win(dealer);
  else if (dealer.score > 21) win(player);
  else if (player.score === 21) win(player);
  else if (dealer.score === 21) win(dealer);
  else if (end && player.score === dealer.score) win(null);
};

/**
 * Draws a card for specified player
 * @param {Player} p Player to draw a card for
 */
const drawCard = (p: Player) => {
  // Draw new card
  // tslint:disable-next-line: no-bitwise
  let card: Card = CARDS.splice((Math.random() * CARDS.length)|0, 1)[0];
  // Save dealer from busting
  if (p === dealer && p.score >= 17) { console.log('No draw'); }
  else {
    // Add player draw number
    p.draws++;
    // Add card to player cards array
    p.cards.push(card);
    // Add score to player and set Ace value
    p.score + card.getValue() > 11 && card.face === 'A'
      ? (p.score += card.getValue())
      : (p.score += card.getValue(false));
  }

  render();
  checkWinner();
};

/*
===============================================================================
Event listeners
===============================================================================
*/
$btn.addEventListener('click', () => {
  drawCard(player);
  drawCard(dealer);
});

$end.addEventListener('click', () => {
  end = true;

  while (dealer.score < 17) drawCard(dealer);

  // Check winner rq
  player.score < dealer.score ? win(dealer) : win(player);
  if (player.score === dealer.score) win(null);

  $score.innerHTML = $score.innerHTML
    .replace(' - Won - Won', ' - Won')
    .replace(' - Won - Lost', ' - Won')
    .replace(' - Lost - Lost', ' - Lost')
    .replace(' - Draw - Won - Draw', ' - Draw')
    .replace(' - Won - Draw', ' - Draw');
});
