/*
===============================================================================
Variables
===============================================================================
*/
var SCORE_MAX = 21;
// Number of decks to play with
var DECK_NUM = 1;
var end = false;
var SUITS = ['♠', '♥', '♣', '♦'];
var FACES = [
    'A',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    'J',
    'Q',
    'K',
];
/*
===============================================================================
DOM elements
===============================================================================
*/
var $d = document;
var $btn = $d.querySelector('#b');
var $end = $d.querySelector('#e');
$end.disabled = true;
var $score = $d.querySelector('#score');
var $cards = $d.querySelector('#cards');
/**
 * Card class
 */
var Card = /** @class */ (function () {
    /**
     * Card class constructor
     * @param {Suit} suit Suit of the card
     * @param {Face} face Face of the card
     */
    function Card(suit, face) {
        var _this = this;
        /**
         * Get card value from face of card
         * @param {boolean} isAceOne Choose if Ace should be counted as 1 or 11
         */
        this.getValue = function (isAceOne) {
            if (isAceOne === void 0) { isAceOne = true; }
            switch (_this.face) {
                case 'A':
                    return isAceOne ? 1 : 11;
                case 'J':
                case 'Q':
                case 'K':
                    return 10;
                default:
                    return parseInt(_this.face);
            }
        };
        this.suit = suit;
        this.face = face;
    }
    return Card;
}());
// Generate card deck(s)
var CARDS = [];
for (var i = 0; i < DECK_NUM * FACES.length; i++)
    for (var j = 0; j < DECK_NUM * SUITS.length; j++) {
        CARDS.push(new Card(SUITS[j], FACES[i]));
    }
/**
 * Player class
 */
var Player = /** @class */ (function () {
    function Player() {
        var _this = this;
        /**
         * Build card display string
         */
        this.display = function () {
            var out = '';
            _this.cards.forEach(function (card) {
                out += card.suit + card.face + ' ';
            });
            return out;
        };
        this.score = 0;
        this.cards = [];
        this.draws = 0;
    }
    return Player;
}());
var player = new Player();
var dealer = new Player();
/*
===============================================================================
Functions
===============================================================================
*/
/**
 * Reset the game after it's over
 */
var reset = function () {
    player = new Player();
    dealer = new Player();
    end = false;
    $score.innerHTML = '0 - 0';
    $cards.innerHTML = 'x - x';
    $btn.innerHTML = 'Draw a card';
    $btn.removeEventListener('click', reset);
};
/**
 * Render scores and enabled end button
 */
var render = function () {
    $cards.innerHTML = player.display() + ' - ' + dealer.display();
    $score.innerText = player.score + ' - ' + dealer.score;
    if (player.score > 0)
        $end.disabled = false;
};
/**
 * Renders won, lost or draw string and disables draw and end buttons
 * @param {Player} winner Winner
 */
var win = function (winner) {
    winner
        ? ($score.innerHTML += winner === player ? ' - Won' : ' - Lost')
        : ($score.innerHTML += ' - Draw');
    $end.disabled = true;
    $btn.innerHTML = 'Reset';
    $btn.addEventListener('click', reset);
};
/**
 * Checks to determine the winner
 */
var checkWinner = function () {
    if (player.score > 21)
        win(dealer);
    else if (dealer.score > 21)
        win(player);
    else if (player.score === 21)
        win(player);
    else if (dealer.score === 21)
        win(dealer);
    else if (end && player.score === dealer.score)
        win(null);
};
/**
 * Draws a card for specified player
 * @param {Player} p Player to draw a card for
 */
var drawCard = function (p) {
    // Draw new card
    // tslint:disable-next-line: no-bitwise
    var card = CARDS.splice((Math.random() * CARDS.length) | 0, 1)[0];
    // Save dealer from busting
    if (p === dealer && p.score >= 17) {
        console.log('No draw');
    }
    else {
        // Add player draw number
        p.draws++;
        // Add card to player cards array
        p.cards.push(card);
        // Add score to player and set Ace value
        p.score + card.getValue() > 11 && card.face === 'A'
            ? (p.score += card.getValue())
            : (p.score += card.getValue(false));
    }
    render();
    checkWinner();
};
/*
===============================================================================
Event listeners
===============================================================================
*/
$btn.addEventListener('click', function () {
    drawCard(player);
    drawCard(dealer);
});
$end.addEventListener('click', function () {
    end = true;
    while (dealer.score < 17)
        drawCard(dealer);
    // Check winner rq
    player.score < dealer.score ? win(dealer) : win(player);
    if (player.score === dealer.score)
        win(null);
    $score.innerHTML = $score.innerHTML
        .replace(' - Won - Won', ' - Won')
        .replace(' - Won - Lost', ' - Won')
        .replace(' - Lost - Lost', ' - Lost')
        .replace(' - Draw - Won - Draw', ' - Draw')
        .replace(' - Won - Draw', ' - Draw');
});
//# sourceMappingURL=script.js.map